# Ancile Extra Utilities

### About
https://gitlab.com/misc-scripts1/windows/ancile/Ancile_ExtraUtilities

These extra utilities are not meant to be included in the general release, and are not guaranteed to function as expected or to be complete.
It is not recomended that you use these scripts unless you are the person who wrote them.

This is a plugin that may or may not require Ancile_Core (https://gitlab.com/misc-scripts1/windows/ancile/Ancile_Core) 

For more information go to https://voat.co/v/ancile